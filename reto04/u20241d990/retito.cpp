#include <iostream>
#include <conio.h>
#include <cstdlib>
#include <ctime>

using namespace std;

int* generarArregloAleatorio(int& tama�o) {
	srand(time(0));

	tama�o = rand() % 401 + 100;
	
	int* arreglo = new int[tama�o];

	for (int i = 0; i < tama�o; ++i) {
		arreglo[i] = rand() % 10000 + 1;
	}
	return arreglo;
}

void probargenerador() {
	int tama�o;
	int* arreglo = generarArregloAleatorio(tama�o);

	cout << "El tama�o del arreglo es: " << tama�o << endl;
	cout << "Elementos del arreglo: " << endl;
	for (int i = 0; i < tama�o; ++i) {
		cout << arreglo[i];
		cout << " ";
	}
	cout << endl;

	delete[] arreglo;
}
int main() {

	probargenerador();

	_getch();
	return 0;
}